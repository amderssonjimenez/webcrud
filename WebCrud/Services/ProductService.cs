﻿using System.Collections.Generic;
using System.Linq;
using WebCrud.Models;
using WebCrud.Models.Entity;

namespace WebCrud.Services
{
    public class ProductService : IRepositoryService<Product>
    {
        private readonly WebCrudContext _dbContext;

        public ProductService(WebCrudContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Add(Product entity)
        {
            _dbContext.Products.Add(entity);
            _dbContext.SaveChanges();
        }

        public void Delete(int id)
        {
            var product = Find(id);
            _dbContext.Products.Remove(product);
            _dbContext.SaveChanges();
        }

        public IEnumerable<Product> SelectAll()
        {
            return _dbContext.Products.ToList();
        }

        public Product Find(int id)
        {
            return _dbContext.Products.Find(id);
        }

        public void Update(Product obj)
        {
            //_dbContext.Entry ;
            _dbContext.SaveChanges();
        }
    }
}