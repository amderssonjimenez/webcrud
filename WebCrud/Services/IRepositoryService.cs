﻿using System.Collections.Generic;

namespace WebCrud.Services
{
    public interface IRepositoryService<T>
    {
        IEnumerable<T> SelectAll();

        T Find(int id);

        void Add(T entity);

        void Update(T entity);

        void Delete(int id);


    }


}
