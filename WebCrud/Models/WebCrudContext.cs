﻿using System.Data.Entity;
using WebCrud.Models.Entity;

namespace WebCrud.Models
{
    public class WebCrudContext : DbContext
    {
        public WebCrudContext() : base("DefaultConnection")
        {
            // DBInitializer startegy
        }

        public DbSet<Product> Products { get; set; }

    }
}