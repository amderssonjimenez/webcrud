﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebCrud.Models.Entity
{
    public class Product
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public DateTime Dateline { get; set; }

        [Range(0, 50)]
        public int Quantity { get; set; }
    }
}