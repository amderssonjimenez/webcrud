﻿using System.Web.Http;

namespace WebCrud.Controllers
{
    [Route("api/product")]
    public class ProductController : ApiController
    {
        // GET: api/Product
        public IHttpActionResult Get()
        {
            return Ok(new string[2] { "demo1", "demo2" });
        }

        //// GET: api/Product/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST: api/Product
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT: api/Product/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE: api/Product/5
        //public void Delete(int id)
        //{
        //}
    }
}
