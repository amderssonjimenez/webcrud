﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Repository
{
    public class Repository<T> : IRepository<T> where T: class
    {
        private DbContext _context = null;
        private DbSet<T> table = null;

        public Repository(DbContext context)
        {
            _context = context;
            table = _context.Set<T>();
        }

        public virtual T GetById(int id)
        {
            return table.Find(id);
        }

        public void Insert(T obj)
        {
            table.Add(obj);
        }

        public void Update(T obj)
        {
            table.Attach(obj);
            _context.Entry(obj).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            T existing = table.Find(id);
            table.Remove(existing);
        }

        public void Save() => _context.SaveChanges();


        IEnumerable<T> IRepository<T>.GetAll()
        {
            table.ToList();
        }
    }
}

